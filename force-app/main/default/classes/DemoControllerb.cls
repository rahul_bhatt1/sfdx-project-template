/**
 * @description       : 
 * @author            : Rahul Bhatt
 * @group             : 
 * @last modified on  : 07-21-2021
 * @last modified by  : Rahul Bhatt
 * Modifications Log 
 * Ver   Date         Author        Modification
 * 1.0   02-09-2021   Rahul Bhatt   Initial Version
**/
public with sharing class DemoControllerb {
    /**
     * An empty constructor for the testingB
     * new changes
     */
    public DemoControllerb() {}

    /**
     * Get the version of the SFDX demo app
     */
    public String getAppVersion() {
        return '1.0.0';
    }
}