/**
 * @description       : 
 * @author            : Rahul Bhatt
 * @group             : 
 * @last modified on  : 04-05-2021
 * @last modified by  : Rahul Bhatt
 * Modifications Log 
 * Ver   Date         Author        Modification
 * 1.0   02-12-2021   Rahul Bhatt   Initial Version
 * new changes
**/
@isTest
private class DemoControllerTestsb {
    @isTest
    static void testGetAppVersion() {
        DemoControllerb demo = new DemoControllerb();
        System.assertEquals(demo.getAppVersion(), '1.0.0');
    }
}