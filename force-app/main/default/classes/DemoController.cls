/**
 * @description       : 
 * @author            : Rahul Bhatt
**/
public with sharing class DemoController {
    /**
     * An empty constructor for the testingA
     */
    public DemoController() {}

    /**
     * Get the version of the SFDX demo app
     */
    public String getAppVersion() {
        List<Account> accList = [SELECT Id, Delete__c FROM Account LIMIT 1];
        return '1.0.0';
    }
}