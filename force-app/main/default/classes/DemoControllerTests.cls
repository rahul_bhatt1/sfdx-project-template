/**
 * @description       : 
 * @author            : Rahul Bhatt
 * @group             : 
 * @last modified on  : 07-29-2021
 * @last modified by  : Rahul Bhatt
**/
@isTest
private class DemoControllerTests {
    @isTest
    static void testGetAppVersion() {
        DemoController demo = new DemoController();
        System.assertEquals(demo.getAppVersion(), '1.0.0');
    }
}